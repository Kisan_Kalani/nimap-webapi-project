﻿using Nimap.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.DataAccess.Context
{
    public class NimapDbContext : DbContext
    {
        public NimapDbContext() : base("NimapWebApi")
        {

        }


        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRole { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<NimapDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }




        //enable-migrations
        //-ContextProjectName "Nimap.DataAccess" 
        //    -StartUpProjectName "Nimap.DataAccess"
        //    -ContextTypeName "Nimap.DataAccess.Context.NimapDbContext"
        //-ProjectName "Nimap.WebApi"

    }
}
