﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.DataAccess.Models
{
    [Table("CategoryMaster")]
    public  class Category
    {
        [Key]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }


        [ForeignKey("User")]
        public int UserId { get; set; }

        public User User { get; set; }


    }
}
