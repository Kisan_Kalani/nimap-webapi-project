﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.DataAccess.Models
{
    [Table("UserRoleMaster")]
    public class UserRole
    {


        [Key, Column(Order = 1)]
        [ForeignKey("user")]
        public int UserId { get; set; }
        public User user { get; set; }

        [Key, Column(Order = 2)]
        [ForeignKey("role")]
        public int RoleId { get; set; }
        public Role role { get; set; }



    }
}
