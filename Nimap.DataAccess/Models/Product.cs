﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.DataAccess.Models
{
    [Table("ProductMaster")]
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [DisplayName("Product Name")]
        public string ProductName { get; set; }


        public int Price { get; set; }


        [DisplayName("Product Image")]
        public string ProdImg { get; set; }



        [ForeignKey("Category")]
        public int CatId { set; get; }

        public Category Category { get; set; }


        [ForeignKey("User")]
        public int UserId { get; set; }

        public User User { get; set; }


    }
}
