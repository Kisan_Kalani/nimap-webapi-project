﻿
using Nimap.Core.Auth;
using Nimap.Core.Role;
using Nimap.DataAccess.Models;
using ServiceModel.Account;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Nimap.WebApi.Controllers
{
    [CustomAuthenticationFilter(RoleHelper.Admin)]
    public class RoleController : ApiController
    {
        private readonly IRole _role;

        public RoleController(IRole role)
        {
            _role = role;
        }


        public IHttpActionResult Get()
        {

            var roles = _role.GetAll();

            if (roles != null)
            {
                return Ok(roles);
            }
            else
            {
                return NotFound();
            }
        }


        [ResponseType(typeof(Role))]
        public IHttpActionResult Get(int id)
        {
            var role = _role.GetById(id);

            if (role != null)
            {
                return Ok(role);
            }
            else
            {
                return NotFound();
            }
        }




        [ResponseType(typeof(Role))]
        public IHttpActionResult Post([FromBody]RoleService roleService)
        {
          
                Role role = new Role()
                {
                    Name = roleService.Name

                };

                _role.Create(role);
                int i = _role.Complete();
                if (i > 0)
                {
                    return CreatedAtRoute("DefaultApi", new { id = role.Id }, role);
                }
                else
                {
                    return BadRequest("Role not Created");
                }
          
        }


        [ResponseType(typeof(Role))]
        public IHttpActionResult Put([FromBody]RoleService roleService)
        {


            var existingRole = _role.GetById(roleService.Id);

                if (existingRole != null)
                {
                    existingRole.Name = roleService.Name;

                    _role.Update(existingRole);
                    int i = _role.Complete();

                    if (i > 0)
                    {
                        return CreatedAtRoute("DefaultApi", new { id = existingRole.Id }, existingRole);
                    }
                    else
                    {

                        return BadRequest("Role not updated");
                    }


                }
                else
                {
                    return NotFound();
                }
           

        }



        public IHttpActionResult Delete(int Id)
        {
            try
            {

                if (Id < 0)
                {
                    return BadRequest("Not a Valid Role ID");
                }

                var role = _role.GetById(Id);

                if (role != null)
                {
                    _role.Delete(role.Id);
                    int i = _role.Complete();


                    if (i > 0)
                    {
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Role cannot be deleted ");
                    }
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception e)
            {

                return BadRequest(e.StackTrace);
            }
        }






    }
}
