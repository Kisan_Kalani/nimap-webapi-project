﻿using Nimap.Core.Auth;
using Nimap.Core.Category;
using Nimap.Core.User;
using Nimap.DataAccess.Models;
using ServiceModel.Category;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Nimap.WebApi.Controllers
{
    public class CategoryController : ApiController
    {
        
        private readonly ICategory _category;
        private readonly IUser _user;

        public CategoryController(ICategory category,IUser user)
        {
            _category = category;
            _user = user;
        }


        [CustomAuthenticationFilter(RoleHelper.Admin)]
        public IHttpActionResult Get(string query = null)
        {

            var categoryList = _category.GetAll("User");


            if (categoryList != null)
            {
                if (!String.IsNullOrEmpty(query))
                {
                       categoryList = _category.SearchBy(c => c.CategoryName.ToLower().Contains(query.ToLower()),"User");
                }
                return Ok(categoryList.ToList());
            }
            else
            {
                return BadRequest();
            }
        }



        [CustomAuthenticationFilter(RoleHelper.Admin)]
        [ResponseType(typeof(Category))]
        public IHttpActionResult Get(int id)
        {
            var category = _category.GetById(id, "User");

            if (category != null)
            {
                return Ok(category);
            }
            else
            {
                return NotFound();
            }

        }



        [CustomAuthenticationFilter(RoleHelper.Admin)]
        [ResponseType(typeof(Category))]
        public IHttpActionResult Post([FromBody]CategoryService category)
        {
            var user = _user.GetById(category.UserId);
           
            if (user != null)
            {
                Category cat = new Category() {
                CategoryName=category.CategoryName,
                UserId= category.UserId
                
                };

                _category.Create(cat);
               
                int i = _category.Complete();
                if (i > 0)
                {
                   
                    return CreatedAtRoute("DefaultApi", new { id = cat.CategoryId }, cat);
                }
                else
                {
                    return BadRequest("Category NOt Created");
                }
            }
            else
            {
                return BadRequest("Check User Id or User Details");
            }

        }


        [CustomAuthenticationFilter(RoleHelper.Admin)]
        [ResponseType(typeof(Category))]
        public IHttpActionResult Put([FromBody]CategoryService category)
        {
            var cat = _category.GetById(category.CategoryId);
            var user = _user.GetById(category.UserId);
         
            if (cat != null && user != null)
            {
                cat.CategoryName = category.CategoryName;
                cat.UserId = category.UserId;
               
                _category.Update(cat);

                int i = _category.Complete();
                if (i > 0)
                {
                    return CreatedAtRoute("DefaultApi", new { id = cat.CategoryId }, cat);
                }
                else
                {
                    return BadRequest("Category Not Updated");
                }
            }
            else
            {
                return NotFound();
            }


        }


        [CustomAuthenticationFilter(RoleHelper.Admin)]
        public IHttpActionResult Delete(int Id)
        {
            try
            {
                if (Id <= 0)
                {
                    return BadRequest("Not a valid Category id");
                }

                var cat = _category.GetById(Id);
                if (cat != null)
                {
                    _category.Delete(cat.CategoryId);
                    int i = _category.Complete();

                    if (i > 0)
                    {
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Unable to Delete Category");
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest("Cannot delete Category as it is Linked to A Product \n " + ex);
            }
        }






    }
}
