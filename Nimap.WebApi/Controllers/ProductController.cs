﻿using Nimap.Core.Auth;
using Nimap.Core.Category;
using Nimap.Core.Product;
using Nimap.Core.User;
using Nimap.DataAccess.Models;
using ServiceModel.Product;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Nimap.WebApi.Controllers
{
    public class ProductController : ApiController
    {
       
        private readonly IProduct _product;
        private readonly ICategory _category;
        private readonly IUser _user;

        public ProductController(IProduct product,ICategory category,IUser user)
        {
            _product = product;
            _category = category;
            _user = user;
        }

        [CustomAuthenticationFilter(RoleHelper.Supervisor, RoleHelper.Customer)]
        public IHttpActionResult Get(string query = null)
        {
            var productList = _product.GetAll("Category,User");

            if (productList != null)
            {
                if (!String.IsNullOrEmpty(query))
                {
                    productList = _product.SearchBy(c => c.ProductName.ToLower().Contains(query.ToLower()), "Category,User");
                }
                return Ok(productList.ToList());
            }
            else
            {
                return BadRequest("No Products");
            }
        }


        [CustomAuthenticationFilter(RoleHelper.Supervisor, RoleHelper.Customer)]
        public IHttpActionResult Get(int id)
        {
            var product = _product.GetById(id, "Category,User");
           
            if (product != null)
            {
                return Ok(product);
            }
            else
            {
                return NotFound();
            }



        }



        [CustomAuthenticationFilter(RoleHelper.Supervisor)]
        [ResponseType(typeof(Product))]
        public IHttpActionResult Post([FromBody]ProductService product)
        {

            var category = _category.GetById(product.CatId);
            var user = _user.GetById(product.UserId);
            
            if (category == null || user == null)
            {
                return BadRequest("Category or User Not Found");
            }
            else
            {
                Product prod = new Product() {
                ProductName = product.ProductName,
                Price = product.Price,
                ProdImg = product.ProdImg,
                UserId =product.UserId,
                CatId=product.CatId
                
                };

                _product.Create(prod);

                int i = _product.Complete();
                if (i > 0)
                {
                    return CreatedAtRoute("DefaultApi", new { id = prod.ProductId }, prod);
                }
                else
                {
                    return BadRequest("Product Not Created");
                }
            }
        }


        [CustomAuthenticationFilter(RoleHelper.Supervisor)]
        [ResponseType(typeof(Product))]
        public IHttpActionResult Put([FromBody]ProductService product)
        {

            var prod = _product.GetById(product.ProductId);
            var cat = _category.GetById(product.CatId);
            var user = _user.GetById(product.UserId);
            
            
            if (prod != null)
            {
                if (cat != null && user != null)
                {
                    prod.ProductName = product.ProductName;
                    prod.Price = product.Price;
                    prod.ProdImg = product.ProdImg;
                    prod.UserId = product.UserId;
                    prod.CatId = product.CatId;
                   
                    _product.Update(prod);
                    int i = _product.Complete();
                    if (i > 0)
                    {
                        return CreatedAtRoute("DefaultApi", new { id = prod.ProductId }, prod);
                    }
                    else
                    {
                        return BadRequest("Product  Not Updated");
                    }

                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return NotFound();
            }

        }


        [CustomAuthenticationFilter(RoleHelper.Supervisor)]
        public IHttpActionResult Delete(int Id)
        {

            if (Id <= 0)
            {
                return BadRequest("Not a valid Product id");
            }
            var product = _product.GetById(Id);
          
            if (product != null)
            {
                _product.Delete(product.ProductId);
                int i = _product.Complete();

                if (i > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Unable to Delete Product");
                }
            }
            else
            {
                return NotFound();
            }
        }


    }
}
