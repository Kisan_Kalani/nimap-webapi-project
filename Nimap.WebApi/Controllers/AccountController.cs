﻿
using Nimap.Core.JWT;

using Nimap.Core.User;
using Nimap.DataAccess.Models;
using ServiceModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Nimap.WebApi.Controllers
{

    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {

        private readonly IUser _user;

        public AccountController(IUser user)
        {
            _user = user;
        }


        [HttpPost]
        [Route("Register")]
        public IHttpActionResult Register(RegisterService model)
        {

            var userAlreadyRegistered = _user.FindByEmail(model.Email);
           
            if (userAlreadyRegistered != null)
            {
                return BadRequest("User Already Registered with Email ");
            }

            if (model != null)
            {
                User user = new User()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Mobile = model.Mobile,
                    Password = model.Password

                };
                

                _user.Create(user);
                int i = _user.Complete();

                if (i > 0)
                {
                    return Ok(user);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest("Registeration Failed");
            }
        }

        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public IHttpActionResult Login(LoginService login)
        {


            var user = _user.CheckUser(login.Email, login.Password);

            if (user != null)
            {
                JwtTokenService jwtTokenService = new JwtTokenService();

                var token = jwtTokenService.GenerateToken(login.Email);
                return Ok(token);


            }
            else
            {
                return BadRequest();

            }

        }


    }
}
