﻿namespace Nimap.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryMaster",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(nullable:false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId)
                .ForeignKey("dbo.UserMaster", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserMaster",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Mobile = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductMaster",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(nullable: false),
                        Price = c.Int(nullable: false),
                        ProdImg = c.String(),
                        CatId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.CategoryMaster", t => t.CatId, cascadeDelete: false)
                .ForeignKey("dbo.UserMaster", t => t.UserId, cascadeDelete: false)
                .Index(t => t.CatId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.RoleMaster",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoleMaster",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.RoleMaster", t => t.RoleId, cascadeDelete: false)
                .ForeignKey("dbo.UserMaster", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoleMaster", "UserId", "dbo.UserMaster");
            DropForeignKey("dbo.UserRoleMaster", "RoleId", "dbo.RoleMaster");
            DropForeignKey("dbo.ProductMaster", "UserId", "dbo.UserMaster");
            DropForeignKey("dbo.ProductMaster", "CatId", "dbo.CategoryMaster");
            DropForeignKey("dbo.CategoryMaster", "UserId", "dbo.UserMaster");
            DropIndex("dbo.UserRoleMaster", new[] { "RoleId" });
            DropIndex("dbo.UserRoleMaster", new[] { "UserId" });
            DropIndex("dbo.ProductMaster", new[] { "UserId" });
            DropIndex("dbo.ProductMaster", new[] { "CatId" });
            DropIndex("dbo.CategoryMaster", new[] { "UserId" });
            DropTable("dbo.UserRoleMaster");
            DropTable("dbo.RoleMaster");
            DropTable("dbo.ProductMaster");
            DropTable("dbo.UserMaster");
            DropTable("dbo.CategoryMaster");
        }
    }
}
