﻿using Autofac;
using Autofac.Integration.WebApi;
using Nimap.Core.Category;
using Nimap.Core.Product;
using Nimap.Core.Repository;
using Nimap.Core.Role;
using Nimap.Core.User;
using Nimap.Core.UserRole;
using Nimap.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace Nimap.WebApi.App_Start
{
    public class AutofacConfig
    {

        public static IContainer Container;


        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }


        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register your Web API controllers.  
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));
            builder.RegisterType(typeof(NimapDbContext));
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));

            builder.RegisterType<CategoryRepository>().As<ICategory>();
            builder.RegisterType<ProductRepository>().As<IProduct>();
            builder.RegisterType<UserRepository>().As<IUser>();
            builder.RegisterType<RoleRepository>().As<IRole>();
            builder.RegisterType<UserRoleRepository>().As<IUserRole>();
            //Set the dependency resolver to be Autofac.  
            Container = builder.Build();

            return Container;
        }
    }
}