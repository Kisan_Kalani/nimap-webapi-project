﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceModel.Role
{
    public static class RoleHelper
    {
        public const string Admin = "Admin";
        public const string Supervisor = "Supervisor";
        public const string Customer = "Customer";
    }
}
