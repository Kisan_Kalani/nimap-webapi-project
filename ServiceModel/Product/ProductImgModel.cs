﻿using ServiceModel.Account;
using ServiceModel.Category;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceModel.Product
{
    public class ProductImgModel
    {
        public int ProductId { get; set; }

        [Required]
        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        [Required]
        public int Price { get; set; }


        [Required]
        [ForeignKey("Category")]
        [DisplayName("Category Name")]
        public int CatId { set; get; }

        public CategoryService Category { get; set; }

        [Required]
        [ForeignKey("User")]
        [DisplayName("User Name")]
        public int UserId { set; get; }

        public UserService User { get; set; }

        [DisplayName("Product Image")]
        public string ProdImg { get; set; }



    }
}
