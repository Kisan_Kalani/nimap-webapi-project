﻿using ServiceModel.Account;
using ServiceModel.Category;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ServiceModel.Product
{
    public class ProductService
    {
        public int ProductId { get; set; }

        [Required]
        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        [Required]
        public int Price { get; set; }

        
        [DisplayName("Product Image")]
        public string ProdImg { get; set; }


        [Required]
        [ForeignKey("Category")]
        [DisplayName("Category Name")]
        public int CatId { set; get; }

        public CategoryService Category { get; set; }

        [Required]
        [ForeignKey("User")]
        [DisplayName("User Name")]
        public int UserId { set; get; }

        public UserService User { get; set; }

        public IEnumerable<SelectListItem> CategoryDDList { get; set; }
        public IEnumerable<SelectListItem> UserDDList { get; set; }


        [DisplayName("Current Image")]
        public string ExistingPhoto { set; get; }

        
        [DisplayName("Product Image")]
        public HttpPostedFileBase ImageFile { set; get; }
    }
}
