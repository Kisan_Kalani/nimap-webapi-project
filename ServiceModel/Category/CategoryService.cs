﻿using ServiceModel.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ServiceModel.Category
{
    public class CategoryService
    {
        public int CategoryId { get; set; }


        [Required]
        [DisplayName("Category Name")]
        public string CategoryName { get; set; }


        [Required]
        [DisplayName("Created By")]
        [ForeignKey("User")]
        public int UserId { get; set; }
        public UserService User { get; set; }

        public string UserEmail { get; set; }

        public int PTotal { get; set; }

        public bool isSelected { get; set; }



        public IEnumerable<SelectListItem> UserDDList { get; set; }


    }
}
