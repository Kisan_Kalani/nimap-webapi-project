﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceModel.Account
{
    public class UserService
    {

        public int Id { get; set; }

        [Required]
        [DisplayName("First Name")]

        public string FirstName { get; set; }


        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [DisplayName("Email")]

        public string Email { get; set; }

        [Required]
        [DisplayName("Password")]

        public string Password { get; set; }

        [Required]
        [DisplayName("Mobile")]

        public string Mobile { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
