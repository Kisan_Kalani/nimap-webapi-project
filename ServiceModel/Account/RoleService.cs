﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceModel.Account
{
    public class RoleService
    {

        public int Id { get; set; }


        [Required]
        [DisplayName("Role Name")]
        public string Name { get; set; }

    }
}
