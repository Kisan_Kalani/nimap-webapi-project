﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceModel.Account
{
    public class RegisterService
    {
        [Required]
        [DisplayName("First Name")]
        public string FirstName { get; set; }


        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DisplayName("Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Pass and Confirm Pass Must be same")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DisplayName("Mobile")]
        [MinLength(10)]
        [StringLength(10, ErrorMessage = "Only 10 Digits")]
        public string Mobile { get; set; }

        [Required(ErrorMessage ="Please select Role")]
        public string RoleName { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }


    }
}
