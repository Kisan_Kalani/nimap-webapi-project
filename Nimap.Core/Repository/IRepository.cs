﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.Repository
{
    public interface IRepository<T> where T : class
    {

        IQueryable<T> GetAll(string includeproperties = "");


        void Create(T obj);

        T GetById(int id,string includeproperties = "");

        IQueryable<T> SearchBy(Expression<Func<T, bool>> predicate,string include="");


        void Update(T obj);


        void Delete(params Object[] id);


        int Complete();
    }
}
