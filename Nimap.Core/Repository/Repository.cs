﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Nimap.Core.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {


        private DbContext _db;
        private DbSet<T> _dbSet;


        public Repository(DbContext db)
        {
            _db = db;
            _dbSet = _db.Set<T>();
        }

        public int Complete()
        {
            return _db.SaveChanges();
        }

        public void Create(T obj)
        {
            try
            {
                _dbSet.Add(obj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete(params object[] id)
        {
            try
            {
                T getObjById = _dbSet.Find(id);
                _dbSet.Remove(getObjById);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public IQueryable<T> SearchBy(Expression<Func<T, bool>> predicate, string include = "")
        {
            try
            {
                var findByWhere = GetAll(include).Where(predicate);
                return findByWhere;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public IQueryable<T> GetAll(string includeProperties = "")
        {
            try
            {
                IQueryable<T> query = _dbSet;

                if (includeProperties != null)
                {
                    foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        query = query.Include(includeProperty);
                    }
                }

                return query.AsNoTracking();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public T GetById(int id, string includeProp = "")
        {

            try
            {
                T getObjById = _dbSet.Find(id);

                if (getObjById != null)
                {
                    if (includeProp != null)
                    {
                        foreach (var includeProperty in includeProp.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {

                            _db.Entry(getObjById).Reference(includeProperty).Load();

                        }
                    }
                }

                return getObjById;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Update(T obj)
        {
            try
            {
                //_db.Entry(obj).State = EntityState.Detached;
                _db.Entry(obj).State = EntityState.Modified;
            }
            catch (Exception e)
            {
                e.StackTrace.ToString();
            }
        }



    }
}
