﻿using Nimap.Core.Repository;
using Nimap.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.Category
{
    public class CategoryRepository : Repository<Nimap.DataAccess.Models.Category>, ICategory
    {
        private readonly NimapDbContext _db;
        public CategoryRepository(NimapDbContext db) : base(db)
        {
            _db = db;
        }

        public void Trial()
        {
            throw new NotImplementedException();
        }
    }
}
