﻿using Nimap.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.Category
{
    public interface ICategory : IRepository<Nimap.DataAccess.Models.Category>
    {
        void Trial();
    }

}
