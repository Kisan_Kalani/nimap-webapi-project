﻿using Microsoft.IdentityModel.Tokens;
using Nimap.Core.Repository;
using Nimap.Core.Role;
using Nimap.Core.User;
using Nimap.Core.UserRole;
using Nimap.DataAccess.Context;
using Nimap.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.JWT
{
    public  class JwtTokenService
    {

        //https://localhost:44365/ MVC
        //https://localhost:44361/ Web api

        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";
        private const string IssueR = "https://localhost:44365";// mvc
        private const string AudiencE = "https://localhost:44361";// api

        //private readonly IUser _user;
        //private readonly IRole _role;
        //private readonly IUserRole _userRole;

        private readonly NimapDbContext _db;
        public JwtTokenService()
        {
            _db = new NimapDbContext();
            //_user = new UserRepository(new NimapDbContext());
            //_role = new RoleRepository(new NimapDbContext());
            //_userRole = new UserRoleRepository(new NimapDbContext());
           
        }




        public string GenerateToken(string username)
        {
            int expireMin = 7200;
            string token = string.Empty;


            var user = _db.Users.Where(u => u.Email.ToLower() == username.ToLower()).FirstOrDefault();
                        //_user.GetAll().Where(u => u.Email.ToLower() == username.ToLower()).FirstOrDefault();

            if (user != null)
            {
                var symmetricKey = Convert.FromBase64String(Secret);
                var tokenHandler = new JwtSecurityTokenHandler();

                var now = DateTime.UtcNow;

                var userIdentity = new ClaimsIdentity();
                List<Claim> claims = new List<Claim>();

                claims.Add(new Claim(ClaimTypes.Name, user.Email));
                claims.Add(new Claim(ClaimTypes.Email, user.Email));


                var userRole = _db.UserRole.Where(u => u.UserId == user.Id).ToList();
                                //_userRole.GetAll().Where(u => u.UserId == user.Id).ToList();


                foreach (var role in userRole)
                {
                    var roleT = _db.Roles.Where(r => r.Id == role.RoleId).FirstOrDefault();
                                //_role.GetAll().Where(r => r.Id == role.RoleId).FirstOrDefault();
                    claims.Add(new Claim(ClaimTypes.Role, roleT.Name));
                }

                userIdentity.AddClaims(claims);


                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Issuer = IssueR,
                    Audience = AudiencE,
                    Subject = userIdentity,
                    NotBefore = now,
                    Expires = now.AddMinutes(Convert.ToInt32(expireMin)),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256)

                };

                var stoken = tokenHandler.CreateToken(tokenDescriptor);
                token = tokenHandler.WriteToken(stoken);


            }

            return token;


        }



        public ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(Secret);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }

            catch (Exception)
            {
                return null;
            }
        }


        public bool ValidateToken(string token, out string username)
        {
            username = null;

            var identity = new ClaimsIdentity();
            if (token != null)
            {
                var simplePrinciple = GetPrincipal(token);
                identity = simplePrinciple.Identity as ClaimsIdentity;
            }

            if (identity == null)
            { return false; }

            if (!identity.IsAuthenticated)
            { return false; }

            var usernameClaim = identity.FindFirst(ClaimTypes.Email);
            username = usernameClaim?.Value;


            if (string.IsNullOrEmpty(username))
            {
                return false;
            }

            return true;
        }

        public  ClaimsPrincipal AuthenticateJwtToken(string token)//Task<IPrincipal>
        {
            string username;

            if (ValidateToken(token, out username))
            {
                // based on username to get more information from database in order to build local identity

                
                var useR = _db.Users.Where(u => u.Email == username).FirstOrDefault();
                            //_user.GetAll().Where(u => u.Email == username).FirstOrDefault();
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, useR.Email));
                claims.Add(new Claim(ClaimTypes.Email, useR.Email));

                var userRole = _db.UserRole.Where(u => u.UserId == useR.Id).ToList();
                                //_userRole.GetAll().Where(u => u.UserId== useR.Id).ToList();


                foreach (var role in userRole)
                {
                    var roleT =   _db.Roles.Where(r => r.Id == role.RoleId).FirstOrDefault();
                                    //_role.GetAll().Where(r => r.Id == role.RoleId).FirstOrDefault();
                    claims.Add(new Claim(ClaimTypes.Role, roleT.Name));
                }


                var identity = new ClaimsIdentity(claims, "Jwt");

                ClaimsPrincipal user = new ClaimsPrincipal(identity);

                return user;
            }

            return null;
        }

    }
}
