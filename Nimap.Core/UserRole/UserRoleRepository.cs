﻿using Nimap.Core.Repository;
using Nimap.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.UserRole
{
    public class UserRoleRepository :Repository<Nimap.DataAccess.Models.UserRole>,IUserRole
    {
        private readonly NimapDbContext _db;
        public UserRoleRepository(NimapDbContext db):base(db)
        {
            _db = db;
        }
    }
}
