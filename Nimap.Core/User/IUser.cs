﻿using Nimap.Core.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.User
{
    public interface IUser : IRepository<Nimap.DataAccess.Models.User>
    {
        Nimap.DataAccess.Models.User FindByEmail(string email);

        Nimap.DataAccess.Models.User CheckUser(string email, string password);
    }
}
