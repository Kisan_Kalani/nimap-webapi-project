﻿using Nimap.Core.Repository;
using Nimap.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.User
{
    public class UserRepository : Repository<Nimap.DataAccess.Models.User>, IUser
    {

        private readonly NimapDbContext _db;

        public UserRepository(NimapDbContext db) : base(db)
        {
            _db = db;
        }

        public DataAccess.Models.User CheckUser(string email, string password)
        {
            var user =_db.Users.Where(u => u.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && u.Password == password)
                                .FirstOrDefault();
            return user;
        }

        public DataAccess.Models.User FindByEmail(string email)
        {
           var user = _db.Users.Where(x => x.Email.Equals(email, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

             return user;
        }
    }

}
