﻿using Nimap.Core.JWT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace Nimap.Core.Auth
{
    public class CustomAuthenticationFilter : AuthorizeAttribute, IAuthenticationFilter
    {


        private readonly string[] _roles;

        public CustomAuthenticationFilter(params string[] roles)
        {
            _roles = roles;


        }


        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            string authParameter = string.Empty;
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authorization = request.Headers.Authorization;

            if (authorization == null)
            {
                context.ErrorResult = new AuthenticationFailureResult("Misssing Authorization Header", request);
                return;
            }
            if (authorization.Scheme != "Bearer")
            {
                context.ErrorResult = new AuthenticationFailureResult("Invalid Authorization Schema!!", request);
                return;
            }
            if (string.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing Token", request);
                return;
            }


            JwtTokenService jwtTokenService = new JwtTokenService();

            context.Principal = jwtTokenService.AuthenticateJwtToken(authorization.Parameter);

        }

        public async Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            var result = await context.Result.ExecuteAsync(cancellationToken);

            if (result.StatusCode == HttpStatusCode.Unauthorized)
            {
                result.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue("Basic", "realm=localhost"));
            }

            context.Result = new ResponseMessageResult(result);

        }


        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            bool IsValid = false;
            var currentIdentity = actionContext.RequestContext.Principal.Identity;

            if (!currentIdentity.IsAuthenticated)
            {
                IsValid = false;
            }
            var user = HttpContext.Current.User;

            var roles = _roles;
            if (roles != null)
            {
                foreach (var role in roles)
                {
                    if (user.IsInRole(role))
                    {
                        IsValid = true;
                    }
                }
            }

            return IsValid;

        }



    }

    public class AuthenticationFailureResult : IHttpActionResult
    {
        public string _responsePhrase;
        public HttpRequestMessage _request { set; get; }

        public AuthenticationFailureResult(string responsePhrase, HttpRequestMessage request)
        {
            _responsePhrase = responsePhrase;
            _request = request;
        }


        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }


        public HttpResponseMessage Execute()
        {
            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            response.RequestMessage = _request;
            response.ReasonPhrase = _responsePhrase;
            return response;
        }
    }
}
