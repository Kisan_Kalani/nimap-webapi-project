﻿using Nimap.Core.Repository;
using Nimap.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.Role
{
    public class RoleRepository : Repository<Nimap.DataAccess.Models.Role>, IRole
    {

        private readonly NimapDbContext _db;

        public RoleRepository(NimapDbContext db) : base(db)
        {
            _db = db;
        }

        public DataAccess.Models.Role FindByName(string roleName)
        {
            try
            {
                var role = _db.Roles.Where(r => r.Name == roleName).FirstOrDefault();
                return role;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
