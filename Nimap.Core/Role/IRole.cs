﻿using Nimap.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.Role
{
    public interface IRole : IRepository<Nimap.DataAccess.Models.Role>
    {
        Nimap.DataAccess.Models.Role FindByName(string roleName);
    }
}
