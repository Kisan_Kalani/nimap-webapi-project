﻿using Nimap.Core.Repository;
using Nimap.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap.Core.Product
{
    public class ProductRepository : Repository<Nimap.DataAccess.Models.Product>, IProduct
    {
        private readonly NimapDbContext _db;
        public ProductRepository(NimapDbContext db) : base(db)
        {
            _db = db;
        }

    }

}
