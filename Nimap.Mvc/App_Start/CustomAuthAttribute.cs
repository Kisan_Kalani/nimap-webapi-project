﻿using Nimap.Core.JWT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.App_Start
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class CustomAuthAttribute : AuthorizeAttribute
    {

        public readonly string[] allowedroles;


        public CustomAuthAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isAuthorize = false;


            if (HttpContext.Current.Session["Token"] != null)
            {
                var token = httpContext.Session["Token"].ToString();
                JwtTokenService jwt = new JwtTokenService();
                var claimsPrincipal = jwt.GetPrincipal(token);

                if (claimsPrincipal.Identity.IsAuthenticated)
                {
                    var roles = allowedroles;

                    if (roles != null)
                    {
                        foreach (var role in roles)
                        {
                            if (claimsPrincipal.HasClaim(x => x.Type == ClaimTypes.Role && x.Value == role) &&
                               claimsPrincipal.HasClaim(x => x.Type == ClaimTypes.Email && x.Value == claimsPrincipal.Identity.Name))
                            {
                                isAuthorize = true;
                                break;
                            }
                            else
                            {
                                isAuthorize = false;
                            }
                            
                        }
                    }

                }
                else
                {
                    isAuthorize = false;
                }
            }
            else
            {
                isAuthorize = false;
            }


            return isAuthorize;
        }

        
    }
}