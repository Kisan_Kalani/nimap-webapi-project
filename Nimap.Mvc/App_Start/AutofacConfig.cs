﻿using Autofac;
using Autofac.Integration.Mvc;
using Nimap.Core.Category;
using Nimap.Core.Product;
using Nimap.Core.Repository;
using Nimap.Core.Role;
using Nimap.Core.User;
using Nimap.Core.UserRole;
using Nimap.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.App_Start
{
    public class AutofacConfig
    {
        public static void RegisterComponents()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType(typeof(NimapDbContext));
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));

            builder.RegisterType<CategoryRepository>().As<ICategory>();
            builder.RegisterType<ProductRepository>().As<IProduct>();
            builder.RegisterType<UserRepository>().As<IUser>();
            builder.RegisterType<RoleRepository>().As<IRole>();
            builder.RegisterType<UserRoleRepository>().As<IUserRole>();

            IContainer container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }

    }
}