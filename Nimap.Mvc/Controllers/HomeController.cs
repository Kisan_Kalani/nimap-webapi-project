﻿using Nimap.Mvc.App_Start;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.Controllers
{
    public class HomeController : Controller
    {

        [CustomAuth(RoleHelper.Admin)]
        public ActionResult Admin()
        {
            return View();
        }

        [CustomAuth(RoleHelper.Supervisor)]
        public ActionResult Supervisor()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}