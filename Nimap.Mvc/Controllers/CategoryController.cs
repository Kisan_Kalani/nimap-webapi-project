﻿using Newtonsoft.Json;
using Nimap.Core.User;
using Nimap.Mvc.App_Start;
using Nimap.Mvc.HttpModel;
using PagedList;
using ServiceModel.Category;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.Controllers
{
    [CustomAuth(RoleHelper.Admin)]
   
    public class CategoryController : Controller
    {

        private readonly IUser _user;
        public CategoryController(IUser user)
        {
            _user = user;
        }


        public async Task<ActionResult> CategoryList(string search, int? page, int? pageSize)
        {

            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Category?query=" + search).Result;

            if (response.IsSuccessStatusCode)
            {

                string resultString = await response.Content.ReadAsStringAsync();
                List<CategoryService> catList = JsonConvert.DeserializeObject<List<CategoryService>>(resultString);


                if (catList != null)
                {
                    if (TempData["CatErr"] != null)
                    {
                        ViewBag.Status = "Cannot delete Category with ID : " + TempData["CatErr"].ToString() + " ,Category  is Linked to A Product";
                        TempData.Remove("CatErr");
                    }



                    int defaultPageSize = pageSize ?? 3;
                    ViewBag.psize = defaultPageSize;

                    ViewBag.PageSize = new List<SelectListItem>()
                    {
                       new SelectListItem() { Value="3", Text= "3" },
                       new SelectListItem() { Value="10", Text= "10" },
                       new SelectListItem() { Value="15", Text= "15" }

                    };



                    if (search != null)
                    {
                        
                        var data = catList.Where(s => s.CategoryName.StartsWith(search, StringComparison.OrdinalIgnoreCase) || search == null).ToList().ToPagedList(page ?? 1, defaultPageSize);

                        return View(data);
                    }
                    else
                    {
                        return View(catList.ToPagedList(page ?? 1, defaultPageSize));
                    }

                }
                else
                {
                    return RedirectToAction("UnAuthorized");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }



        public async Task<ActionResult> CategoryDetails(int id)
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Category/" + id.ToString()).Result;

            if (response.IsSuccessStatusCode)
            {
                string resultString = await response.Content.ReadAsStringAsync();
                CategoryService category = JsonConvert.DeserializeObject<CategoryService>(resultString);
                if (category != null)
                {
                    return View(category);
                }
                else
                {
                    return RedirectToAction("UnAuthorized");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }


        [HttpGet]
        public ActionResult CategoryCreate()
        {
            CategoryService category = new CategoryService();
            category.UserDDList = UserList();

            return View(category);
        }


        [HttpPost]
        public ActionResult CategoryCreate(CategoryService category)
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage response = HttpClientAccess.webApiClient.PostAsJsonAsync("Category", category).Result;
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("CategoryList");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }

            }
            else
            {
                return View();
            }
            // return View();
        }



        [HttpGet]
        public async Task<ActionResult> CategoryEdit(int id)
        {

            CategoryService category = new CategoryService();
           
            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Category/" + id.ToString()).Result;
            if (response.IsSuccessStatusCode)
            {
                string resultString = await response.Content.ReadAsStringAsync();
                category = JsonConvert.DeserializeObject<CategoryService>(resultString);
                category.UserDDList = UserList();
                return View(category);
            }
            else
            {
                return RedirectToAction("Login", "Account");

            }
        }

        [HttpPost]
        public async Task<ActionResult> CategoryEdit(CategoryService categor)
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage response = HttpClientAccess.webApiClient.PutAsJsonAsync<CategoryService>("Category", categor).Result;

                if (response.IsSuccessStatusCode)
                {
                    string resultString = await response.Content.ReadAsStringAsync();
                    CategoryService category = JsonConvert.DeserializeObject<CategoryService>(resultString);

                    if (category != null)
                    {
                        return RedirectToAction("CategoryList");
                    }
                    else
                    {
                        return RedirectToAction("UnAuthorized");
                    }

                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            return View();
        }



        [HttpPost]
        public ActionResult CategoryDelete(int id)
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.DeleteAsync("Category/" + id.ToString()).Result;

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("CategoryList");
            }
            else
            {
                TempData["CatErr"] = id.ToString();
                return RedirectToAction("CategoryList");
            }
        }



        public ActionResult UnAuthorized()
        {
            return View();
        }


        [NonAction]
        public List<SelectListItem> UserList()
        {
            List<SelectListItem> userSe = new List<SelectListItem>();
            var userList = _user.GetAll();
            foreach (var item in userList)
            {
                userSe.Add(new SelectListItem() { Text = item.FirstName + " " + item.LastName, Value = item.Id.ToString() });
            }
            return userSe;
        }
    }
}