﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Nimap.Core.Product;
using Nimap.DataAccess.Models;
using ServiceModel.Order;
using ServiceModel.Product;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly IProduct _product;
        public ShoppingCartController(IProduct product)
        {
            _product = product;
        }

        [HttpPost]
        public ActionResult AddToCart(int id)
        {
            OrderService model = new OrderService();
            //var prodFromDb = _product.GetAll("Category,User").FirstOrDefault(p => p.ProductId == id);

            var prodFromDb = _product.GetById(id, "Category,User");
            ProductService prod = new ProductService()
            {
                ProductId = prodFromDb.ProductId,
                ProductName = prodFromDb.ProductName,
                Price = prodFromDb.Price,
                ProdImg = prodFromDb.ProdImg,
                CatId = prodFromDb.CatId,
                UserId = prodFromDb.UserId
            };

            if (Session["cart"] == null)
            {

                model.li = new List<ProductService>();
                model.li.Add(prod);

                Session["cart"] = model.li;
                ViewBag.cart = model.li.Count();

                Session["count"] = 1;



            }
            else
            {


                model.li = (List<ProductService>)Session["cart"];
                model.li.Add(prod);



                Session["cart"] = model.li;
                ViewBag.cart = model.li.Count();
                Session["count"] = Convert.ToInt32(Session["count"]) + 1;

            }
            return RedirectToAction("ProductList", "Product");

        }



        public ActionResult Myorder()
        {
            OrderService model = new OrderService();
            model.li = (List<ProductService>)Session["cart"];

            return View(model);

        }


        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {

            Product  prod = _product.GetAll("Category,User").FirstOrDefault(p => p.ProductId == id);
            //OrderService model = new OrderService();
            var li = (List<ProductService>)Session["cart"];

            li.RemoveAll(x => x.ProductId == prod.ProductId);


            Session["cart"] = li;
            Session["count"] = Convert.ToInt32(Session["count"]) - 1;

            int cou = Convert.ToInt32(Session["count"]);
            if (cou > 0)
            {
                return RedirectToAction("Myorder", "ShoppingCart");
            }
            return RedirectToAction("ProductList", "Product");
        }

        #region CreatePDF


        public FileResult CreatePdf(int sum)
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            // file name to be created 
            string strPDFFileName = string.Format("MyOrderPdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            Document doc = new Document();
            doc.SetMargins(10f, 10f, 10f, 10f);
            // Create PDF Table with 5 coulmns
            PdfPTable tableLayout = new PdfPTable(3);
            //doc.SetMargins(0f, 0f, 0f, 0f);

            // Create PDF Table

            // file will created in this path
            string strAttachment = Server.MapPath("~/Downloads" + strPDFFileName);

            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();

            //Add content to PDF
            doc.Add(Add_Content_To_PDF(tableLayout, sum));

            // closing the document
            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;

            return File(workStream, "application/pdf", strPDFFileName);
        }

        private PdfPTable Add_Content_To_PDF(PdfPTable tableLayout, int sum)
        {
            //float[] headers = { 50, 24, 45, 35, 50 };   // header widths
            float[] headers = { 50, 45, 24 };   // header widths
            tableLayout.SetWidths(headers);             // Set the pdf headers
            tableLayout.WidthPercentage = 100;
            tableLayout.HeaderRows = 1;

            // Add Title to the PDF file at the Top

            //List<Product> product = db.Products.ToList<Product>();
            //var invoice = (List<Product>)Session["cart"];
            OrderService model = new OrderService();



            model.li = (List<ProductService>)Session["cart"];
            model.total = sum;
            var invoice = model;
            var user = (User)Session["User"];

            tableLayout.AddCell(new PdfPCell(new Phrase("InVoice Of Placed Orders", new Font(Font.FontFamily.HELVETICA, 8, 1,
                new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            tableLayout.AddCell(new PdfPCell(new Phrase("Customer Name  :   " + user.Email.ToString(), new Font(Font.FontFamily.HELVETICA, 8, 1,
               new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 2, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Contact Number :   " + user.Mobile.ToString(), new Font(Font.FontFamily.HELVETICA, 8, 1,
              new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 2, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


            AddCellToHeader(tableLayout, "Product Id");
            AddCellToHeader(tableLayout, "Product Name");
            AddCellToHeader(tableLayout, "Product Price");

            // Add Body
            if (invoice != null)
            {
                foreach (var pro in invoice.li)
                {
                    AddCellToBody(tableLayout, pro.ProductId.ToString());
                    AddCellToBody(tableLayout, pro.ProductName);
                    AddCellToRupee(tableLayout, pro.Price.ToString());
                }

            }


            AddCellToFinalAmt(tableLayout, invoice.total.ToString());

            //tableLayout.AddCell(new PdfPCell(new Phrase("Total Amount                   Rs. " + invoice.total.ToString() + "/-", new Font(Font.FontFamily.HELVETICA, 8, 1,
            //    new BaseColor(0, 0, 0))))
            //{ Colspan = 3, Border = 2, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            tableLayout.AddCell(new PdfPCell(new Phrase("*****************THANK YOU FOR SHOPPING*****************", new Font(Font.FontFamily.HELVETICA, 8, 1,
                 new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            // AddCellToBody(tableLayout, );
            return tableLayout;
        }

        // Method to add single cell to the Header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {

            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.YELLOW))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new BaseColor(128, 0, 0) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {

            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.BLACK)))
            { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new BaseColor(255, 255, 255) });
        }

        private static void AddCellToRupee(PdfPTable tableLayout, string cellText)
        {
            BaseFont bf = BaseFont.CreateFont("C:/Windows/Fonts/Arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(bf, 8, 1, BaseColor.BLACK);
            Chunk chunkRupee = new Chunk("\u20B9" + " " + cellText, font);
            tableLayout.AddCell(new PdfPCell(new Phrase(chunkRupee)) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new BaseColor(255, 255, 255) });
        }

        private static void AddCellToFinalAmt(PdfPTable tableLayout, string cellText)
        {
            BaseFont bf = BaseFont.CreateFont("C:/Windows/Fonts/Arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(bf, 18, 1, new BaseColor(0, 0, 0));
            Chunk chunkRupee = new Chunk("Total Amount Is \u20B9 " + cellText + "\\-", font);
            tableLayout.AddCell(new PdfPCell(new Phrase(chunkRupee)) { Colspan = 3, Border = 2, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
        }
        #endregion





    }
}