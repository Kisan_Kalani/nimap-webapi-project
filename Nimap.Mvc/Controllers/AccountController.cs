﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Nimap.Core.JWT;
using Nimap.Core.Role;
using Nimap.Core.User;
using Nimap.Core.UserRole;
using Nimap.DataAccess.Models;
using Nimap.Mvc.HttpModel;
using ServiceModel.Account;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly IUser _user;
        private readonly IRole _role;
        private readonly IUserRole _userRole;
        public AccountController(IUser user,IRole role,IUserRole userRole)
        {
            _user = user;
            _role = role;
            _userRole = userRole;
        }


        public ActionResult Register()
        {
            RegisterService model = new RegisterService();
            return View(model);
        }

        [HttpPost]
        public ActionResult Register(RegisterService model)
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.PostAsJsonAsync("Account/Register", model).Result;
            if (response.IsSuccessStatusCode)
            {


                var role = _role.FindByName(model.RoleName);
                var user = _user.FindByEmail(model.Email);
               
                UserRole ur = new UserRole()
                {
                    RoleId = role.Id,
                    UserId= user.Id
                };

                _userRole.Create(ur);
                int i  = _userRole.Complete();
                if (i > 0)
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    ViewBag.Error = "Registeration Failed";
                    return View();
                }
                
            }
            else
            {
                ViewBag.Error = "Registeration Failed";
                return View();
            }

        }


        [HttpGet]
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginService model, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                string tokenbased = string.Empty;
                var response = HttpClientAccess.webApiClient.PostAsJsonAsync("Account/Login", model).Result;
                if (response.IsSuccessStatusCode)
                {
                    var respn = response.Content.ReadAsStringAsync().Result;
                    tokenbased = JsonConvert.DeserializeObject<string>(respn);
                    
                    Session["Token"] = tokenbased;
                    var loggedInUser = _user.FindByEmail(model.Email);
                    Session["User"] = loggedInUser;

                    JwtTokenService jwt = new JwtTokenService();
                    

                    HttpClientAccess.webApiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Session["Token"].ToString());

                    
                    return RedirectToLocal(ReturnUrl);
                }
                else
                {
                    ModelState.AddModelError("er", "Invalid User Email OR Password");
                    return View();
                }
            }
            return View();
        }


        public ActionResult Logout()
        {

            if (!String.IsNullOrEmpty(HttpContext.Session["Token"].ToString()))
            {
                Session.Remove("Token");
                Session.Remove("User");
                Session.Remove("Role");


                HttpClientAccess.webApiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", null);

            }
            return View("Login");
        }


        [NonAction]
        private ActionResult RedirectToLocal(string returnurl)
        {
            
            if (returnurl == null)
            {
                if (HttpContext.Session["Token"].ToString() != null)
                {
                    JwtTokenService jwt = new JwtTokenService();
                    string token = HttpContext.Session["Token"].ToString();
                    var prin = jwt.GetPrincipal(token);
                    var ident = prin.Identity as ClaimsIdentity;

                    if (ident.HasClaim(ClaimTypes.Role, RoleHelper.Admin))
                    {

                        return RedirectToAction("Admin", "Home");
                    }else if(ident.HasClaim(ClaimTypes.Role, RoleHelper.Supervisor))
                    {
                        Session["Role"] = RoleHelper.Supervisor;
                        return RedirectToAction("Supervisor", "Home");
                    }
                    else
                    {
                        return RedirectToAction("ProductList", "Product");
                    }
                }
               
            }

            if (!String.IsNullOrEmpty(returnurl) && Url.IsLocalUrl(returnurl))
            {
                return Redirect(returnurl);
            }

            return View("Login", "Account");
        }


    }
}