﻿using Newtonsoft.Json;
using Nimap.Core.Role;
using Nimap.Core.User;
using Nimap.Core.UserRole;
using Nimap.DataAccess.Models;
using Nimap.Mvc.App_Start;
using Nimap.Mvc.HttpModel;
using ServiceModel.Account;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.Controllers
{
    [CustomAuth(RoleHelper.Admin)]
    public class RoleController : Controller
    {
        private readonly IRole _role;
        private readonly IUser _user;
        private readonly IUserRole _userRole;
        public RoleController(IRole role,IUser user,IUserRole userRole)
        {
            _role = role;
            _user = user;
            _userRole = userRole;
        }

        public async Task<ActionResult> RoleList()
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Role").Result;

            if (response.IsSuccessStatusCode)
            {
                string resultString = await response.Content.ReadAsStringAsync();
                List<RoleService> roleList = JsonConvert.DeserializeObject<List<RoleService>>(resultString);

                if (roleList != null)
                {
                    if (TempData["roleErr"] != null)
                    {
                        ViewBag.Status = "Cannot delete Role with ID : " + TempData["roleErr"].ToString() + " ,Role is Linked to User";
                        TempData.Remove("roleErr");
                    }

                    return View(roleList);
                }
                else
                {
                    return RedirectToAction("UnAuthorized");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }



        [HttpGet]
        public ActionResult RoleCreate()
        {
            RoleService role = new RoleService();
            return View(role);
        }



        [HttpPost]
        public ActionResult RoleCreate(RoleService role)
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage response = HttpClientAccess.webApiClient.PostAsJsonAsync("Role", role).Result;
                if (response.IsSuccessStatusCode)
                {


                    return RedirectToAction("RoleList");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }

            }
            else
            {
                return View();
            }

        }



        [HttpGet]
        public async Task<ActionResult> RoleEdit(int id)
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Role/" + id.ToString()).Result;
            if (response.IsSuccessStatusCode)
            {
                string resultString = await response.Content.ReadAsStringAsync();
                RoleService role = JsonConvert.DeserializeObject<RoleService>(resultString);
                return View(role);
            }
            else
            {

            }
            {
                return RedirectToAction("Login", "Account");

            }
        }


        [HttpPost]
        public async Task<ActionResult> RoleEdit(RoleService roleservice)
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage response = HttpClientAccess.webApiClient.PutAsJsonAsync<RoleService>("Role", roleservice).Result;

                if (response.IsSuccessStatusCode)
                {
                    string resultString = await response.Content.ReadAsStringAsync();
                    RoleService role = JsonConvert.DeserializeObject<RoleService>(resultString);

                    if (role != null)
                    {
                        return RedirectToAction("RoleList");
                    }
                    else
                    {
                        return RedirectToAction("UnAuthorized");
                    }

                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult RoleDelete(int id)
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.DeleteAsync("Role/" + id.ToString()).Result;

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("RoleList");
            }
            else
            {
                TempData["roleErr"] = id.ToString();
                return RedirectToAction("RoleList");
            }
        }


        [HttpGet]
        public ActionResult AddRoleToUsers(int id)
        {

            ViewBag.RoleId = id;

            //var role = _role.GetAll().FirstOrDefault(r => r.Id == id);
            var role = _role.GetById(id);

            ViewBag.RoleName = role.Name;

            var model = new List<UserRoleService>();

            var users = _user.GetAll().ToList();

            foreach (var user in users)
            {
                UserRoleService userRoleModel = new UserRoleService()
                {
                    UserId = user.Id,
                    UserName = user.Email,

                };

                var userRole = _userRole.GetAll().FirstOrDefault(u => u.UserId == user.Id);

                if (userRole != null)
                {
                    if (role.Id == userRole.RoleId)
                    {
                        userRoleModel.IsSelected = true;
                    }
                    else
                    {
                        userRoleModel.IsSelected = false;
                    }
                }
                else
                {
                    userRoleModel.IsSelected = false;
                }


                model.Add(userRoleModel);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AddRoleToUsers(List<UserRoleService> model, int roleid)
        {
            if (model != null)
            {
                var role = _role.GetById(roleid);
                //var role = _role.GetAll().FirstOrDefault(r => r.Id == roleid);

                for (int i = 0; i < model.Count; i++)
                {
                    //var user = _user.GetAll().FirstOrDefault(u => u.Id == model[i].UserId);
                    var user = _user.GetById(model[i].UserId);

                    if (model[i].IsSelected && (!CheckUserInRole(user.Id, role.Id)))
                    {
                        UserRole ur = new UserRole()
                        {
                            UserId = user.Id,
                            RoleId = role.Id
                        };

                        _userRole.Create(ur);
                        int user_added_to_role = _userRole.Complete();

                    }
                    else if ((!model[i].IsSelected) && (CheckUserInRole(user.Id, role.Id)))
                    {
                        //var ur = _userRole.GetAll().Where(u => u.UserId == user.Id && u.RoleId == role.Id).FirstOrDefault();
                        var ur = _userRole.SearchBy(u => u.UserId == user.Id && u.RoleId == role.Id).FirstOrDefault();
                        _userRole.Delete(ur.UserId, ur.RoleId);
                        int user_removed_to_role = _userRole.Complete();


                    }
                    else
                    {
                        continue;
                    }

                    if (i < (model.Count - 1))
                    {
                        continue;
                    }
                    else
                    {
                        return RedirectToAction("Rolelist");
                    }

                }



            }
            return RedirectToAction("Rolelist");
        }


        [NonAction]
        public bool CheckUserInRole(int userId, int roleId)
        {
            //var role = _role.GetAll().FirstOrDefault(u => u.Id == roleId);
            var role = _role.GetById(roleId);
            var userRoles = _userRole.GetAll().FirstOrDefault(u => u.UserId == userId);
            if (role != null && userRoles != null)
            {

                if (userRoles.RoleId == role.Id)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        public ActionResult UnAuthorized()
        {
            return View();
        }
    }
}