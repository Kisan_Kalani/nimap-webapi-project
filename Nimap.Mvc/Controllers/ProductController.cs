﻿using Newtonsoft.Json;
using Nimap.Core.Category;
using Nimap.Core.User;
using Nimap.DataAccess.Models;
using Nimap.Mvc.App_Start;
using Nimap.Mvc.HttpModel;
using PagedList;
using ServiceModel.Category;
using ServiceModel.MultipleImages;
using ServiceModel.Product;
using ServiceModel.Role;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Nimap.Mvc.Controllers
{
    public class ProductController : Controller
    {
        private readonly ICategory _category;
        private readonly IUser _user;

        public ProductController(ICategory category,IUser user)
        {
            _category = category;
            _user = user;
        }

        
        #region CRUD

        [CustomAuth(RoleHelper.Supervisor,RoleHelper.Customer)]
        public async Task<ActionResult> ProductList(string search, int? page, int? pageSize,int? minPrice ,int? maxPrice, List<CategoryService> cateDataQuery)
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Product?query=").Result;

            if (response.IsSuccessStatusCode)
            {

                string resultString = await response.Content.ReadAsStringAsync();
                List<ProductService> prodList = JsonConvert.DeserializeObject<List<ProductService>>(resultString);


                if (prodList != null)
                {

                    #region Page Size

                    int defaultPageSize = pageSize ?? 3;
                    ViewBag.psize = defaultPageSize;

                    ViewBag.PageSize = new List<SelectListItem>()
                    {
                       new SelectListItem() { Value="3", Text= "3" },
                       new SelectListItem() { Value="10", Text= "10" },
                       new SelectListItem() { Value="15", Text= "15" }

                    };
                    #endregion

                    #region Category Filter
                    
                    var categoryList = new List<CategoryService>();
                    if (cateDataQuery != null)
                    {

                        if (cateDataQuery.Count > 0)
                        {
                            categoryList = cateDataQuery.Where(x => x.isSelected == true).ToList();

                            Session["CatFilter"] = categoryList;

                        }
                    }
                    else
                    {
                        Session.Remove("CatFilter");
                    }

                    categoryList = Session["CatFilter"] as List<CategoryService>;

                    var CatList = _category.GetAll().ToList();
                    List<CategoryService> modelList = new List<CategoryService>();
                    foreach (var cat in CatList)
                    {
                        CategoryService model = new CategoryService();
                        model.CategoryId = cat.CategoryId;
                        model.CategoryName = cat.CategoryName;

                        modelList.Add(model);
                    }

                    var viewDataDict = new ViewDataDictionary(modelList);

                    ViewData["viewDataDict"] = viewDataDict;

                    #endregion


                    if (minPrice != null && maxPrice != null && categoryList.Count > 0)
                    {

                        List<ProductService> FinalList = new List<ProductService>();
                        for (int i = 0; i < categoryList.Count; i++)
                        {
                            var EnumProd =  prodList
                                    .Where(c => c.Category.CategoryName.Equals(categoryList[i].CategoryName, StringComparison.OrdinalIgnoreCase)
                                    &&
                                    (c.Price >= minPrice && c.Price <= maxPrice));

                            FinalList.AddRange(EnumProd);

                        }
                        var data = FinalList.OrderBy(p => p.Price).ToPagedList(page ?? 1, defaultPageSize);
                        return View(data);
                    }


                    if (minPrice != null && maxPrice != null)
                    {
                        var data = prodList.Where(p => p.Price >= minPrice && p.Price <= maxPrice).OrderBy(x => x.Price).ToPagedList(page ?? 1, defaultPageSize);
                        return View(data);
                    }

                    if (categoryList != null)
                    {   
                        List<ProductService> FinalList = new List<ProductService>();

                        for (int i = 0; i < categoryList.Count; i++)
                        {
                            var EnumProd = prodList.Where(c => c.Category.CategoryName.Equals(categoryList[i].CategoryName, StringComparison.OrdinalIgnoreCase));

                            FinalList.AddRange(EnumProd);
                           
                        }
                        var data = FinalList.ToPagedList(page ?? 1, defaultPageSize);

                        return View(data);
                    }


                    if (search != null)
                    {
                        var data = prodList.Where(s => s.ProductName.StartsWith(search, StringComparison.OrdinalIgnoreCase) || search == null).ToList().ToPagedList(page ?? 1, defaultPageSize);


                        return View(data);
                    }
                    else
                    {
                        return View(prodList.ToPagedList(page ?? 1, defaultPageSize));
                    }

                    
                }
                else
                {
                    return RedirectToAction("UnAuthorized");
                }
            }
            else
            {
                return RedirectToAction("UnAuthorized");
            }

        }

       
        
        [CustomAuth(RoleHelper.Supervisor, RoleHelper.Customer)]
        public async Task<ActionResult> ProductDetails(int id)
        {

            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Product/" + id.ToString()).Result;
            if (response.IsSuccessStatusCode)
            {
                string resultString = await response.Content.ReadAsStringAsync();
                ProductService product = JsonConvert.DeserializeObject<ProductService>(resultString);
                return View(product);
            }
            else
            {
                return RedirectToAction("UnAuthorized");
            }

        }



        [CustomAuth(RoleHelper.Supervisor)]
        [HttpGet]
        public ActionResult ProductCreate()
        {
            ProductService prod = new ProductService();
            prod.CategoryDDList = CategoryList();
            prod.UserDDList = UserList();

            return View(prod);
        }




        [CustomAuth(RoleHelper.Supervisor)]
        [HttpPost]
        public   ActionResult ProductCreate(ProductService product)
        {
           
            if (ModelState.IsValid)
            {
                var imgModel = new ProductImgModel();

                //var br = new BinaryReader(product.ImageFile.InputStream);
                
                
                //imgModel.PicData = br.ReadBytes(product.ImageFile.ContentLength);
                imgModel.ProductName = product.ProductName;
                imgModel.CatId = product.CatId;
                imgModel.UserId = product.UserId;
                imgModel.Price = product.Price;
                imgModel.ProdImg = ProcessImage(product);

                

                HttpResponseMessage response = HttpClientAccess.webApiClient.PostAsJsonAsync("Product", imgModel).Result;
                if (response.IsSuccessStatusCode)
                {
                    
                    return RedirectToAction("ProductList");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            {
                return View();
            }
           
        }



        [CustomAuth(RoleHelper.Supervisor)]
        [HttpGet]
        public async Task<ActionResult> ProductEdit(int id)
        {
            ProductService prod = new ProductService();
            HttpResponseMessage response = HttpClientAccess.webApiClient.GetAsync("Product/" + id.ToString()).Result;
            if (response.IsSuccessStatusCode)
            {
                string resultString = await response.Content.ReadAsStringAsync();
                prod = JsonConvert.DeserializeObject<ProductService>(resultString);
            }


            prod.CategoryDDList = CategoryList();
            prod.UserDDList = UserList();
            prod.ExistingPhoto = prod.ProdImg;

            return View(prod);


        }




        [CustomAuth(RoleHelper.Supervisor)]
        [HttpPost]
        public async Task<ActionResult> ProductEdit(ProductService product)
        {
            if (ModelState.IsValid)
            {
                var imgModel = new ProductImgModel();

                imgModel.ProductId = product.ProductId;
                imgModel.ProductName = product.ProductName;
                imgModel.Price = product.Price;
                imgModel.CatId = product.CatId;
                imgModel.UserId = product.UserId;

                if (product.ImageFile != null)
                {
                    if (product.ExistingPhoto != null)
                    {
                        string[] fileNameSplit = product.ExistingPhoto.Split('/');
                        string fName = fileNameSplit[fileNameSplit.Length - 1];

                        string filename = Path.Combine(Server.MapPath("~/Image/"), fName);
                        System.IO.File.Delete(filename);
                    }
                  
                    imgModel.ProdImg = ProcessImage(product);

                }
                else
                {
                    imgModel.ProdImg = product.ExistingPhoto;
                }
                HttpResponseMessage response = HttpClientAccess.webApiClient.PutAsJsonAsync("Product", imgModel).Result;

                if (response.IsSuccessStatusCode)
                {
                    string resultString = await response.Content.ReadAsStringAsync();
                    ProductService prod = JsonConvert.DeserializeObject<ProductService>(resultString);
                    if (prod != null)
                    {
                        return RedirectToAction("ProductList");
                    }
                    else
                    {
                        return RedirectToAction("UnAuthorized");
                    }


                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            return View();

        }



        [CustomAuth(RoleHelper.Supervisor)]
        [HttpPost]
        public ActionResult ProductDelete(int id)
        {
            HttpResponseMessage response = HttpClientAccess.webApiClient.DeleteAsync("Product/" + id.ToString()).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("ProductList");
            }
            else
            {
                return RedirectToAction("ProductList");
            }
        }

        public ActionResult UnAuthorized()
        {
            return View();
        }


        #endregion

        #region Multiple Images Upload

        [CustomAuth(RoleHelper.Supervisor)]
        [HttpGet]
        public ActionResult MultipleImages()
        {

            return View();
        }

        [CustomAuth(RoleHelper.Supervisor)]
        [HttpPost]
        public ActionResult MultipleImages(MultipleImagesModel model)
        {
            if (ModelState.IsValid)
            {
                foreach (HttpPostedFileBase image in model.multipleImgs)
                {

                    if (image != null && image.ContentLength <= 10485760)//less than  10MB each
                    {
                        string filename = Path.GetFileNameWithoutExtension(image.FileName);

                        string extension = Path.GetExtension(image.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                        filename = Path.Combine(Server.MapPath("~/Image/"), filename);
                        if (extension == ".png" || extension == ".jpg" || extension == ".jpeg")
                        {
                            image.SaveAs(filename);

                        }
                        else
                        {
                            return View();
                        }
                    }

                }

                return RedirectToAction("ProductList");
            }

            return View();

        }
        #endregion

        #region Import From Excel

        [CustomAuth("Supervisor")]
        [HttpGet]
        public ActionResult ImportFromExcel()
        {
            return View();
        }


        [CustomAuth("Supervisor")]
        [HttpPost]
        public ActionResult ImportFromExcel(HttpPostedFileBase postedFile)
        {
            if (ModelState.IsValid)
            {
                if (postedFile != null && postedFile.ContentLength > (1024 * 1024 * 50))  // 50MB limit  
                {
                    ModelState.AddModelError("postedFile", "Your file is to large. Maximum size allowed is 50MB !");
                }

                else
                {
                    string filePath = string.Empty;

                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    filePath = path + Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName);
                    postedFile.SaveAs(filePath);

                    string conString = string.Empty;

                    switch (extension)
                    {
                        case ".xls": //For Excel 97-03.  
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //For Excel 07 and above.  
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }

                    try
                    {
                        DataTable dt = new DataTable();
                        conString = string.Format(conString, filePath);

                        using (OleDbConnection connExcel = new OleDbConnection(conString))
                        {
                            using (OleDbCommand cmdExcel = new OleDbCommand())
                            {
                                using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                {
                                    cmdExcel.Connection = connExcel;

                                    //Get the name of First Sheet.  
                                    connExcel.Open();
                                    DataTable dtExcelSchema;
                                    dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                    string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();

                                    connExcel.Close();

                                    //Read Data from First Sheet.  
                                    connExcel.Open();

                                    cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                                    odaExcel.SelectCommand = cmdExcel;
                                    odaExcel.Fill(dt);
                                    dt.TableName = sheetName.Replace("$", "");
                                    connExcel.Close();
                                }
                            }
                        }

                        conString = ConfigurationManager.ConnectionStrings["NimapWebApi"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(conString))
                        {
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {


                                //Set the database table name.
                                sqlBulkCopy.DestinationTableName = "ProductMaster";
                                sqlBulkCopy.ColumnMappings.Add("ProductName", "ProductName");
                                sqlBulkCopy.ColumnMappings.Add("Price", "Price");
                                sqlBulkCopy.ColumnMappings.Add("ProdImg", "ProdImg");
                                sqlBulkCopy.ColumnMappings.Add("CatId", "CatId");
                                sqlBulkCopy.ColumnMappings.Add("UserId", "UserId");

                                con.Open();

                                sqlBulkCopy.WriteToServer(dt);

                                con.Close();

                                ViewBag.Result = "Successfully Imported";
                                return RedirectToAction("ImportFromExcel");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ViewBag.error = e.Message + "\n" + e.StackTrace;
                        return View("ImportFromExcel");
                    }

                }


            }

            return View(postedFile);
        }
        #endregion

        #region Non Action Methods
        [NonAction]
        private string ProcessImage(ProductService model)
        {

            if (model.ImageFile != null)
            {
                string filename = Path.GetFileNameWithoutExtension(model.ImageFile.FileName);

                string extension = Path.GetExtension(model.ImageFile.FileName);
                filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                model.ProdImg = "~/Image/" + filename;


                filename = Path.Combine(Server.MapPath("~/Image/"), filename);

                model.ImageFile.SaveAs(filename);


            }


            return model.ProdImg;

        }

        [NonAction]
        public List<SelectListItem> UserList()
        {
            List<SelectListItem> userSe = new List<SelectListItem>();
            var userList = _user.GetAll();
            foreach (var item in userList)
            {
                userSe.Add(new SelectListItem() { Text = item.FirstName + " " + item.LastName, Value = item.Id.ToString() });
            }
            return userSe;
        }

        [NonAction]
        public List<SelectListItem> CategoryList()
        {
            List<SelectListItem> catSe = new List<SelectListItem>();
            var catList = _category.GetAll();
            foreach (var item in catList)
            {
                catSe.Add(new SelectListItem() { Text = item.CategoryName, Value = item.CategoryId.ToString() });
            }

            return catSe;
        }



        #endregion

    }
}