﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Nimap.Mvc.HttpModel
{
    public class HttpClientAccess
    {
        
        public static HttpClient webApiClient = new HttpClient();

        static HttpClientAccess()
        {
            ////https://localhost:44361/api/category

            webApiClient.BaseAddress = new Uri("https://localhost:44361/api/");
            webApiClient.DefaultRequestHeaders.Clear();
            webApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


        }

    }
}